import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import TrackPlayer, {
  useTrackPlayerEvent,
  TrackPlayerEvents,
  STATE_PLAYING,
  STATE_STOPPED
} from 'react-native-track-player'

// Subscribing to the following events inside out component
const events = [
  TrackPlayerEvents.PLAYBACK_STATE,
  TrackPlayerEvents.PLAYBACK_ERROR
]

class Post extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isPlaying: false,
      error: false,
      // trackId: ??,
      // trackObject: ??,
      // buffered: ??,
      position: null,
      duration: null
    }
  }

  async stop () {
    this.setState({
      isPlaying: false
    })
    TrackPlayer.stop()
  }

  async play () {
    TrackPlayer.setupPlayer()
      .then(async () => {
        // Adds a track to the queue
        await TrackPlayer.add({
          id: 'trackId',
          url: this.props.filePath,
          title: 'Track Title',
          artist: 'Track Artist'
        })

        // Starts playing it
        TrackPlayer.play()
        this.setState({ isPlaying: true })

        const listener = TrackPlayer.addEventListener(
          'playback-queue-ended',
          () => {
            listener.remove()
            this.setState({ isPlaying: false })
          }
        )
      })
      .catch(err => this.setState({ error: true }))
  }

  componentDidMount () {
    // Adds an event handler for the playback-track-changed event
    this.onTrackChange = TrackPlayer.addEventListener(
      'playback-track-changed',
      async data => {
        let state = await TrackPlayer.getState()

        let trackId = await TrackPlayer.getCurrentTrack()
        let trackObject = await TrackPlayer.getTrack(trackId)

        // Position, buffered position and duration return values in seconds
        let position = await TrackPlayer.getPosition()
        let buffered = await TrackPlayer.getBufferedPosition()
        let duration = await TrackPlayer.getDuration()
        this.setState({
          // trackId,
          // trackObject,
          position,
          // buffered,
          duration
        })
      }
    )
  }

  componentWillUnmount () {
    // Removes the event handler
    this.onTrackChange.remove()
  }

  render () {
    const { timestamp, author, authorName, filePath } = this.props
    const { isPlaying, error, position } = this.state

    // const mins = Math.floor(length / 60)
    // const secs = Math.floor(length % 60)
    // const time = `${mins < 10 ? 0 : ''}${mins}:${secs < 10 ? 0 : ''}${secs}`

    const publishedAt = new Date(timestamp).toLocaleDateString()

    const buttonStyles = Object.assign({}, styles.playButton, {
      backgroundColor: isPlaying ? '#00ff00' : error ? '#f00' : '#00f'
    })
    return (
      <View style={{ padding: 5, background: 'grey' }}>
        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.author}>{authorName || author}</Text>
        </View>
        <View style={styles.playerContainer}>
          {filePath && (
            <TouchableOpacity
              disabled={error}
              onPress={() => {
                if (isPlaying) {
                  this.stop()
                } else {
                  this.play()
                }
              }}
              style={buttonStyles}
            >
              {!error ? (
                <View style={isPlaying ? styles.square : styles.triangle} />
              ) : (
                <Text style={{ color: '#fff' }}>Error</Text>
              )}
            </TouchableOpacity>
          )}
          <ProgressBar isActive={isPlaying} duration={this.props.duration} />
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={styles.time}>{publishedAt} </Text>
        </View>
      </View>
    )
  }
}

class ProgressBar extends TrackPlayer.ProgressComponent {
  render () {
    const { duration: declaredDuration, isActive } = this.props
    const { position, duration } = this.state

    if (!this.props.isActive) {
      return (
        <View style={styles.progressBar}>
          <Text />
          <View style={styles.progressInactive}>
            <View style={{ height: 3, flex: 100, backgroundColor: '#ddd' }} />
          </View>
          <Text style={{ fontSize: 12 }}>
            {declaredDuration ? formatTime(declaredDuration) : '??'}s
          </Text>
        </View>
      )
    }

    var progress = Math.floor(Math.min(position / duration, 1) * 100)
    if (isNaN(progress)) progress = 0
    if (progress === 100) progress = 0
    console.log('PROGRESS', position, duration)
    return (
      <View style={styles.progressBar}>
        <Text />
        <View style={styles.progressActive}>
          <View
            style={{ height: 3, flex: progress, backgroundColor: '#f0f' }}
          />
          <View
            style={{ height: 3, flex: 100 - progress, backgroundColor: '#ddd' }}
          />
        </View>
        <Text style={{ fontSize: 12 }}>
          {isActive && Math.round(position)}s /
          {declaredDuration ? formatTime(declaredDuration) : '??'}s
        </Text>
      </View>
    )
  }
}

function formatTime (t) {
  return Math.floor(t * 10) / 10
}

const styles = StyleSheet.create({
  playerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: 20
  },
  author: {
    fontWeight: 'bold'
  },
  time: {
    fontWeight: 'normal'
  },
  playButton: {
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderRadius: 50
  },
  triangle: {
    height: 0,
    width: 0,
    borderStyle: 'solid',
    borderColor: 'white',
    borderTopWidth: 5,
    borderRightWidth: 0,
    borderBottomWidth: 5,
    borderLeftWidth: 8,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderRightColor: 'white'
  },
  square: {
    height: 8,
    width: 8,
    backgroundColor: 'white'
  },
  progressBar: {
    flex: 1,
    alignItems: 'flex-end',
    marginLeft: 5
  },
  progressInactive: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  progressActive: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
})

export default Post
