const path = require('path')
const caps = require('ssb-caps')
const Config = require('ssb-config/inject')
let appDataDir

if (!process.env.DESKTOP) {
  const bridge = require('rn-bridge')
  appDataDir = bridge.app.datadir()
} else {
  appDataDir = '/tmp/.ssb-test/'
}

const ssbPath = path.resolve(appDataDir, '.ssb')

const config = (() => {
  const NET_PORT = 26831
  const appName = 'ssb'

  return Config(appName, {
    path: ssbPath,
    connections: {
      incoming: {
        net: [{ scope: 'private', transform: 'shs', port: NET_PORT }]
        // channel: [{ scope: 'device', transform: 'noauth' }]
      },
      outgoing: {
        net: [{ transform: 'shs' }],
        ws: [{ transform: 'shs' }]
      }
    },
    friends: {
      hops: 1
    }
  })
})()

const ssConfig = { caps: { shs: Buffer.from(caps.shs, 'base64') } }

const sbot = require('secret-stack')(ssConfig) // eslint-disable-line
  .use(require('ssb-db'))
  .use(require('ssb-conn'))
  .use(require('ssb-lan'))
  .use(require('ssb-replicate')) // must be loaded before ssb-friends
  .use(require('ssb-friends'))
  .use(require('ssb-blobs'))
  .use(require('ssb-promiscuous'))
  .use(require('ssb-serve-blobs'))
  .use(require('ssb-backlinks'))
  .use(require('ssb-about'))
  .use(require('ssb-ebt'))
  .call(null, config)

if (!process.env.DESKTOP) {
  require('./api')(sbot, appDataDir)
  require('./live-streams')(sbot, appDataDir)
  // NOTE could send a message to front-end saying "READY"
  // to tell UI it's sage to launch + start requesting things
}
