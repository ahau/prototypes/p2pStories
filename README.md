# SSB Starter

# p2pStories

this is a proof of concept app designed to validate whether we can get

- [ ] get Go peer running on Android
- [ ] get Patchql (Rust indexes + graphql server) running
- [ ] build a simple React-Native UI which lets peers:
  - record audio messages
  - see audio messages recorded by others
  - add minimal profile info (name)
  - see / manage basic connectivity (local peers, following)
  - see sync status (am I currently syncing?)
- [ ] android app working with an alt-net (using a different `caps.shs` key)

testing will be done by:
- [ ] running this application on low end Android phones
- [ ] checking this android device will replicate:
  - with modes:
    - [ ] on local Wifi
    - [ ] over internet
  - with different peers:
  - [ ] other android peers
  - [ ] existing desktop peers
  - [ ] iOS peer (verse)
- [ ] testing how long onbording takes
  - [ ] for a light network
  - [ ] for the current ssb main-net

work done on this repo is a collaboration between several projects:
- Āhau (Ben, Mix)
- Brazlian comms project (Luandro)
- APC / San project (Nico, Nic, et al)

## Development

Make sure you're using Node version 10 (use [nvm](https://github.com/nvm-sh/nvm))

1. Install `react-native-cli` : see https://facebook.github.io/react-native/docs/getting-started
2. Clone this repo down and run `npm install`
3. Run `npm run build` to build the NodeJS project, located at `nodejs-assets/nodejs-project/`.
4. Open the `./android` directory on Android Studio to download necessary files.
5. Plug in your Android (with debugging mode enabled) with USB
6. Run `npm run start`
    - this starts the development server, which will live compile on any changes
7. Run `npm run push`
    - this pushs the android app into your phone, and then connected to the dev-server to get the latest contents

All going well you should see the app, and when you make changes to the App UI, then you'll the App update on your phone!

Check out the `package.json` scripts for a range of tools: logging, cache clearing etc

---

### Gotchas

**file watcher limits!!** : with Metro Bundler (the dev server) I hit a file watcher limit. To overcome this I ran : 
```bash
cat /proc/sys/fs/inotify/max_user_watches
sudo sysctl fs.inotify.max_user_watches=16384
sudo sysctl -p
cat /proc/sys/fs/inotify/max_user_watches   
```

here are [some notes](https://confluence.jetbrains.com/display/IDEADEV/Inotify+Watches+Limit) (which I've not tried yet) for how to make that change permanent (and not temporary like the above is) 

I think some apps might also cut into your watcher limit : typescript server, beaker browser (?), ... 


**installing node modules** - if you install a new module in `nodejs-assets/nodejs-project`, you might get errors. Doing a fresh build of the backend seems to fix it :
```bash
rm -rf nodejs-assets/nodejs-project/node_mdules
npm run build
npm run push
```

### Sweet tips

**UI reloading** - you can shake the anroid app once it's loaded and this brings up a developer menu. Click "Enable Live Reload" !

**abd into the phone** - knowing what's going on with your blobs can be important. run the following while phone is connected via USB to have a looks around : 

```bash
adb shell
run-as com.p2pstories
```

then have a look at file usage
```
du -h -lah files
cd files/.ssb
du -h flume
```

## Releasing

Read [the docs](https://facebook.github.io/react-native/docs/signed-apk-android) on how to create a key-pair and config `~/.gradle/gradle.properties`. Run `npm run release` to generate the installables at `android/app/build/outputs/bundle/release`.

