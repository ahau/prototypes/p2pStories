/**
 * @format
 */

import { AppRegistry } from 'react-native'
import App from './components/App'
import { name as appName } from './app.json'
import TrackPlayer from 'react-native-track-player'

AppRegistry.registerComponent(appName, () => App)
// AppRegistry.registerHeadlessTask('TrackPlayer', () =>
//   require('./player-handler.js')
// )
TrackPlayer.registerPlaybackService(() => require('./player-handler'))
